This Drupal 7 module does one basic thing, it removes tabs from the Login /
Create Account / Request Password pages. There is no configuration needed. A use
case for this would be if you want a more elegant and streamlined interface for
these pages and you want to link to the other pages. In this case you would want
your own user-login.tpl.php etc… which takes some extra work. For more and
greater control over tabs, you might want to check out the Tab Tamer Module.
Note this module does not unset the tabs so the pages are still available
providing you have not disabled front end user registration via the Drupal UI.

This module was inspired by a post here: http://drupal.org/node/483324 "Remove
tabs using hook_menu_alter"